const path = require('path')
const webpack = require('webpack')
// 用于合并webpack配置
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const HTMLWebpackePlugin = require('html-webpack-plugin')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')
const NameAllModulesPlugin = require('name-all-modules-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

const isDev = process.env.NODE_ENV === 'development'

const config = webpackMerge(baseConfig, {
  entry: {
    app: path.join(__dirname, '../client/app.js')
  },
  output: {
    filename: '[name].[hash].js'
  },
  plugins: [
    new HTMLWebpackePlugin({
      template: path.join(__dirname, '../client/index.tpl.html')
    })
  ]
})

// webpack-dev-server  是在dist目录下创建的，而在output文件中配置了publicPath: 'public'，
// 所以实际路径为localhost:port/filename [localhost:8888/public]。
// 需要在devServer 中设置publicPath和historyApiFallback 才能将路径改为localhost:filename [localhost:8888/public]。
if (isDev) {
  config.devtool = 'source-map' // 在调试的时候显示的是未打包之前的源代码
  config.entry = [
    'react-hot-loader/patch',
    path.join(__dirname, '../client/app.js')
  ]
  config.module.rules.push(
    {
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [require('autoprefixer')]
            }
          }
        },
        'less-loader'
      ]
    }
  )
  config.devServer = {
    host: '0.0.0.0', // 可以使用ip、127.9.9.1或localhost访问
    port: '2222',
    // contentBase: path.join(__dirname, '../dist'), //本地服务器所加载的页面所在的目录
    hot: true,
    overlay: { // 如果程序在编译过程中报错，直接将错误显示在网页上
      errors: true
    },
    publicPath: '/public/',
    historyApiFallback: {
      index: '/public/index.html' // 所有404的请求都返回设置的这个html文件
    },
    proxy: {
      '/api': 'http://localhost:1234'
    }
  }
  config.stats = { // 终端中输出结果为彩色
    colors: true
  }
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({
      url: 'http://localhost:2222'
    })
  )
} else {
  config.entry = {
    app: path.join(__dirname, '../client/app.js'),
    vendor: [
      'react',
      'react-dom',
      'react-router-dom',
      'axios',
      'query-string'
      // 'dateformat',
    ]
  }
  config.output.filename = '[name].[chunkhash].js'
  config.module.rules.push(
    {
      test: /\.less$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          'css-loader',
          'less-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () { return [require('autoprefixer')] }
            }
          }
        ]
      })
    }
  )
  config.plugins.push(
    new webpack.BannerPlugin('深圳奥琦玮-KDS'),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({ // 将第三方包单独分割打包
      name: 'vendor'
    }),
    new webpack.optimize.CommonsChunkPlugin({ // 即使没有更改任何业务代码，只要重新打包webpack都会在app中生成代码，所以需要将webpack生成的代码单独分割打包
      name: 'manifest',
      minChunks: Infinity
    }),
    new ExtractTextPlugin('[name].[chunkhash:8].css'), // 分离css文件
    new webpack.NamedModulesPlugin(), // webpack异步加载
    new NameAllModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.NamedChunksPlugin((chunk) => { // 具体给每一个chunk打包所命名的名字
      if (chunk.name) {
        return chunk.name
      }
      return chunk.mapModules(m => path.relative(m.context, m.request)).join('_')
    })
  )
}

module.exports = config
