const path = require('path')

module.exports = {
  output: {
    path: path.join(__dirname, '../dist'),
    publicPath: '/public/'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpg|png|gif|svg|ttf|eot|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: '8000',
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(jsx|js)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react', 'stage-1'], // {'loose': true}
          plugins: [
            [
              'transform-runtime', // 代替polyfill，使用 promise,set,map，generator 不报错
              {
                'helpers': true,
                'polyfill': true,
                'regenerator': true,
                'moduleName': 'babel-runtime'
              }
            ],
            'react-hot-loader/babel'
          ]
        }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.less', '.json']
  },
  devtool: 'source-map'
}
