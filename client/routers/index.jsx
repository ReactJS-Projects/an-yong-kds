import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Home from '../views/Home'
import Setting from '../views/Setting'
import PassDish from '../views/PassDish'
import PassDishOrder from '../views/PassDishOrder'
import Nav from '../views/Nav'

// const routesChild = [

// ]

const routes = [
  {
    exact: true,
    path: '/',
    component: Home,
  },
  {
    path: '/setting',
    component: Setting,
  },
  {
    path: '/passdishorder',
    component: PassDishOrder,
  },
  {
    path: '/passdish',
    component: PassDish,
  },
  {
    path: '/nav',
    component: Nav,
  },
]

const routeComponent = routesArgu => (
  routesArgu.map(route => (
    <Route
      key={route.path}
      path={route.path}
      exact={route.exact}
      component={route.component}
    />
  ))
)

export default () => (
  <Switch>
    {routeComponent(routes)}
    {/* <Route render={() => (
      [
        <Header key="header" />,
        routeComponent(routesChild),
      ]
    )} */}
    {/* /> */}
  </Switch>
)
