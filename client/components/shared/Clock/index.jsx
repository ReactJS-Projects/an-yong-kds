import React from 'react';

export default class Clock extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      time: '00:00',
      date: '0000/00/00',
    }
  }

  componentWillMount() {
    clearTimeout(this.time)
    this.start()
  }

  componentWillUnmount() {
    clearTimeout(this.time)
  }

  start() {
    const today = new Date()
    let year = today.getFullYear()
    let month = today.getMonth() + 1
    let day = today.getDate()
    let hours = today.getHours()
    let minutes = today.getMinutes()
    let seconds = today.getSeconds()

    // 如果只有个位，前面补0
    month = month < 10 ? "0" + month : month
    day = day < 10 ? "0" + day : day
    hours = hours < 10 ? "0" + hours : hours
    minutes = minutes < 10 ? "0" + minutes : minutes
    seconds = seconds < 10 ? "0" + seconds : seconds
    this.setState({
      time: hours + ':' + minutes + ':' + seconds,
      date: year + '/' + month + '/' + day,
    })
    // 延时器
    this.time = setTimeout(() => {
      this.start()
    }, 1000)
  }

  render() {
    return (
      <div className="clock">
        <div>{this.state.date}</div>
        <div>{this.state.time}</div>
      </div>
    )
  }
}
