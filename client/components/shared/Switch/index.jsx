import React from 'react'
import './style.less'

const Switch = (props) => {
  const { onClick } = props
  return (
    <div className="switch" onClick={onClick}>
      <span className="off"></span>
    </div>
  )
}

export default Switch
