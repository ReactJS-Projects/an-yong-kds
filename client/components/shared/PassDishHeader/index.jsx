import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../Header'
import './style.less'

export default () => (
  <header className="pass-dish-header">
    <div className="pass-dish"><Link to="/">单品模式</Link></div>
    <Header />
    <div className="done-order"><Link to="/">完成订单</Link></div>
  </header>
)
