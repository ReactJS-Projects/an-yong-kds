import React from 'react'
import { createPortal } from 'react-dom'
import PropTypes from 'prop-types'
import './style.less'

export default class Dialog extends React.Component {
  constructor(props) {
    super(props)
    const $body = document.body
    this.node = document.createElement('div')
    this.node.classList.add('dialog-wrap')
    $body.appendChild(this.node)
    // this.state = {
    //   isOpen: props.isOpen || false,
    // };
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    // if ('isOpen' in nextProps) {
    //   this.setState({
    //     isOpen: nextProps.isOpen,
    //   })
    // }
  }
  componentWillUnmount() {
    window.document.body.removeChild(this.node);
  }
  render() {
    const {
      title,
      children,
      className,
      okText,
      cancelText,
      onOk,
      onCancel,
      // maskClosable,
    } = this.props;

    return (
      createPortal(
        <div className={`dialog-container ${className}`}>
          <div className="dialog-title">
            <p>{title}</p>
            <button>X</button>
          </div>
          <div className="dialog-content">
            {children}
          </div>
          <div className="dialog-footer">
            <button className="ok-btn" onClick={onOk}>{okText}</button>
            <button className="cancel-btn" onClick={onCancel}>{cancelText}</button>
          </div>
        </div>,
        this.node
      )
    )
  }
}

Dialog.propTypes = {
  // isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]).isRequired,
  className: PropTypes.string,
  // maskClosable: PropTypes.bool,
  onCancel: PropTypes.func,
  onOk: PropTypes.func,
  okText: PropTypes.string,
  cancelText: PropTypes.string,
  type: PropTypes.oneOf(['alert', 'confirm', 'error']),
};
Dialog.defaultProps = {
  className: '',
  // maskClosable: true,
  type: 'alert',
  onCancel: () => {},
  onOk: () => {},
  okText: '确定',
  cancelText: '取消',
};
