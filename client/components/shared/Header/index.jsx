import React from 'react'
import { Link } from 'react-router-dom'
import Clock from '../Clock'
import './style.less'

const leftData = [
  {
    name: '刷新',
    type: 'reload',
  },
  {
    name: '备餐',
    type: 'ready',
  },
  {
    name: '叫回',
    type: 'back',
  },
  {
    name: '退出',
    type: 'quit',
  },
]

const Header = () => (
  <div className="tools">
    <ul className="toolBar">
      {
        leftData.map(item => <li key={item.type} type={item.type}>{item.name}</li>)
      }
    </ul>
    <ul className="toolBar">
      <li className="clock-box"><Clock /></li>
      <li><Link to="/setting">设置</Link></li>
    </ul>
  </div>
)

export default Header
