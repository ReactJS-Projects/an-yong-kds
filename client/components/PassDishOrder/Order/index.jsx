import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import Item from './Item'
import './style.less'

const Order = ({options}) => (
  <div className="item-wrap">
    <div className="title info">
      <div>
        <span>434号桌：</span>
        <span>136分钟</span>
      </div>
      <div>@XXX：XXX</div>
      <div>订单号：666466466</div>
      <div className="order-type">堂食</div>
    </div>
    <div className="details-wrap">
      <div className="danger scroll-item">
        <div className="child-scroll-wrap">
          <ReactIscroll
            iScroll={iScroll}
            options={options}
          >
            <div className="child-scroll-inner-box">
              {
                [1, 2, 3, 4, 5, 6, 7, 7, 5].map(() => <Item />)
              }
            </div>
          </ReactIscroll>
        </div>
      </div>
    </div>
  </div>
)

Order.defaultProps = {
  options: {
    mouseWheel: true,
  },
}
Order.propTypes = {
  options: PropTypes.object.isRequired,
}


export default Order
