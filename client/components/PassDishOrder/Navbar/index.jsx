import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import './style.less'

const Navbar = ({options}) => (
  <div className="danger scroll-item">
    <div className="child-scroll-wrap">
      <ReactIscroll
        iScroll={iScroll}
        options={options}
      >
        <div className="child-scroll-inner-box">
          <div>
            <div>123 号桌</div>
            <div>13:58</div>
          </div>
        </div>
      </ReactIscroll>
    </div>
  </div>
)

Navbar.defaultProps = {
  options: {
    mouseWheel: true,
  },
}
Navbar.propTypes = {
  options: PropTypes.object.isRequired,
}

export default Navbar
