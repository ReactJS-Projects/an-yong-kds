import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'

export default class Making extends React.Component {
  componentDidMount() {
    // 站位
  }

  render() {
    return (
      <div className="info scroll-item">
        <div className="child-scroll-wrap">
          <ReactIscroll
            iScroll={iScroll}
            options={this.props.options}
          >
            <div className="child-scroll-inner-box">
              <div>
                <dl>
                  <dt>
                    <p>披萨</p>
                    <p>堂食</p>
                  </dt>
                  <dd>
                    <p>--备注</p>
                    <p>--做法</p>
                  </dd>
                </dl>
              </div>
              <div>开始时间：10:56:65</div>
              <div>
                所属订单：
                <dl>
                  <dt>订单：1</dt>
                  <dd>菜品Item</dd>
                  <dt>订单：2</dt>
                  <dd>菜品Item</dd>
                </dl>
              </div>
            </div>
          </ReactIscroll>
        </div>
      </div>
    )
  }
}

Making.defaultProps = {
  options: {
    mouseWheel: true,
  },
}
Making.propTypes = {
  options: PropTypes.object.isRequired,
}
