import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import Modal from '../../shared/Modal'


export default class WaitMake extends React.Component {
  constructor(props) {
    super(props)
    console.log(1112)
    // this.dialog = this.dialog.bind(this)
  }
  componentDidMount() {
    // 站位
  }

  // dialog() {
  //   console.log(11)
  // }

  render() {
    return (
      <div className="danger scroll-item">
        <div className="child-scroll-wrap">
          <ReactIscroll
            iScroll={iScroll}
            options={this.props.options}
          >
            <div className="child-scroll-inner-box">
              <div>
                <dl>
                  <dt>
                    <p>披萨</p>
                    <p>堂食</p>
                  </dt>
                  <dd>
                    {/* <p>--备注</p>
                    <p>--做法</p> */}
                    <button
                      onClick={() => Modal.alert({
                        title: 'Demo',
                        content: 'Hello world!',
                        okText: '确认',
                        cancelText: '取消',
                        onOk: () => console.log('ok'),
                        onCancel: () => console.log('cancel'),
                      })}
                    >备注
                    </button>
                  </dd>
                </dl>
              </div>
              <div>下单时间12:12:12</div>
              <div>预计时间:13:12:12</div>
              <div>顾客位：5</div>
            </div>
          </ReactIscroll>
        </div>
      </div>
    )
  }
}

WaitMake.defaultProps = {
  options: {
    mouseWheel: true,
  },
}
WaitMake.propTypes = {
  options: PropTypes.object.isRequired,
}
