import React from 'react'
import { push } from 'react-router-redux'

const A = (a) => {
  const { dispatch } = a
  const onClick = () => {
    dispatch(push('/othersss'))
  }

  return <div onClick={onClick}>我是隐藏的点击跳转 --- Home to othersss</div>
}

export default A
