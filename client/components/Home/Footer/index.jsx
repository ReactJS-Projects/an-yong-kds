import React from 'react'
import './style.less'

const data = [
  '烤炉',
  '烤炉1',
  '烤炉2',
]

const Footer = () => (
  <footer>
    <ul>
      {
        data.map(item => (
          <li>
            <span className="badge">{item}</span>
          </li>
        ))
      }
    </ul>
  </footer>
)

export default Footer
