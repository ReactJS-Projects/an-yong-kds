import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'

const PassDish = ({options}) => (
  <div className="col-xs-4 col-sm-4 col-md-4 col-lg-3 col-xl-2 scroll-item-wrap">
    <div className="danger scroll-item">
      <div>
        <dl>
          <dt>
            <p>披萨</p>
            <p>堂食</p>
          </dt>
          <dd>
            <p>--备注</p>
            <p>--做法</p>
          </dd>
        </dl>
      </div>
      <div>送餐时间：12:12:12</div>
      <div>
        <div className="table">桌台号</div>
        <div className="time">12：12</div>
      </div>
    </div>
  </div>
)

PassDish.defaultProps = {
  options: {
    mouseWheel: true,
    startX: 0,
    startY: 0,
  },
}
PassDish.propTypes = {
  options: PropTypes.object.isRequired
}
export default PassDish
