import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

const home = () => {
  console.log('reducer')
  return 'reducer'
}

const reducers = {
  home,
}
export default combineReducers({
  ...reducers,
  router: routerReducer,
});
