import createSagaMiddleware from 'redux-saga'
import { call } from 'redux-saga/effects'

const saga = () => console.log('saga')

export function* rootSaga() {
  yield [
    // takeLatest(saga),
    call(saga),
  ]
}

export default createSagaMiddleware()
