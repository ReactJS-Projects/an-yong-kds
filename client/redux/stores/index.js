import { createStore, applyMiddleware, compose } from 'redux'
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import reducers from '../reducers'
import sagaMiddleware, { rootSaga } from '../sagas'


export const history = createHistory() // 创建您选择的历史
const middleware = routerMiddleware(history)

let store // eslint-disable-line
if (!(window.__REDUX_DEVTOOLS_EXTENSION__ || window.__REDUX_DEVTOOLS_EXTENSION__)) { // eslint-disable-line
  store = createStore(
    reducers,
    applyMiddleware(sagaMiddleware, middleware),
  );
} else {
  store = createStore(
    reducers,
    compose(applyMiddleware(sagaMiddleware, middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()), // eslint-disable-line
  )
}

sagaMiddleware.run(rootSaga)

export default store
