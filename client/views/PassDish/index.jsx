import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import PassDishHeader from '../../components/shared/PassDishHeader'
import PassDishItem from '../../components/PassDish'
import './style.less'

const PassDish = ({options}) => {
  console.log('Order')
  return (
    <div className="container" id="pass-dish">
      <PassDishHeader />
      <div className="main">
        <div className="scroll-wrap">
          <ReactIscroll
            iScroll={iScroll}
            className="iscroll grid-container"
            options={options}
          >
            <div className="scroll-inner-box row">
              {
                [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map(() => <PassDishItem />)
              }
            </div>
          </ReactIscroll>
        </div>
      </div>
    </div>
  )
}

PassDish.defaultProps = {
  options: {
    mouseWheel: true,
  },
}
PassDish.propTypes = {
  options: PropTypes.object.isRequired,
}

export default PassDish
