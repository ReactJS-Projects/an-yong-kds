import React from 'react'
import { Link } from 'react-router-dom'

const links = [
  {
    link: '/',
    name: '主操控界面',
  },
  {
    link: '/setting',
    name: '设置',
  },
  {
    link: '/passdishorder',
    name: '传菜屏-订单模式',
  },
  {
    link: '/passdish',
    name: '传菜屏-单品模式',
  },
]

export default () => links.map(link => <Link style={{'color': '#000', 'display': 'block'}} key={link.link} to={link.link}>{link.name}</Link>)
