import React from 'react'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import PassDishHeader from '../../components/shared/PassDishHeader'
import Navbar from '../../components/PassDishOrder/Navbar'
import Order from '../../components/PassDishOrder/Order'
import './style.less'

const PassDishOrder = ({options}) => {
  console.log('Order')
  return (
    <div className="container" id="pass-dish-order">
      <PassDishHeader />
      <div className="order scroll-wrap">
        <ReactIscroll
          iScroll={iScroll}
          className="iscroll"
          options={options}
        >
          <div className="scroll-inner-box">
            {
              [1, 1, 1, 1, 1, 1, 1].map(() => <Order />)
            }
          </div>
        </ReactIscroll>
      </div>
      <div className="navbar scroll-wrap">
        <ReactIscroll
          iScroll={iScroll}
          className="iscroll"
          options={options}
        >
          <div className="scroll-inner-box">
            {
              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map(() => <Navbar />)
            }
          </div>
        </ReactIscroll>
      </div>
    </div>
  )
}

PassDishOrder.defaultProps = {
  options: {
    mouseWheel: true,
    scrollX: true,
  },
}
PassDishOrder.propTypes = {
  options: PropTypes.object.isRequired,
}

export default PassDishOrder
