import React from 'react'
import { Link } from 'react-router-dom'
import Switch from '../../components/shared/Switch'
import Clock from '../../components/shared/Clock'
import './style.less'

export default class Setting extends React.Component {
  constructor(props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  componentDidMount(props) {
    console.log(props)
  }

  onClick(item) {
    console.log(item)
  }

  render() {
    return (
      <div className="container" id="setting">
        <header>
          <Link to="/">退出设置</Link>
          <div className="clock-box"><Clock /></div>
        </header>
        <div className="setting-list">
          {
            [1, 2, 3, 4, 5, 6].map(item => (
              <div className="setting-item">
                <div className="text">烤炉1</div>
                <Switch
                  onClick={() => {
                    this.onClick(item)
                  }}
                />
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}
