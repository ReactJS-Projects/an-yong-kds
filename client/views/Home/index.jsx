import React from 'react'
import { connect } from 'react-redux'
import ReactIscroll from 'react-iscroll'
import iScroll from 'iscroll'
import PropTypes from 'prop-types'
import Header from '../../components/shared/Header'
import Footer from '../../components/Home/Footer'
import WaitMake from '../../components/Home/WaitMake'
import Making from '../../components/Home/Making'
import './style.less'

class Home extends React.Component {
  componentDidMount() {
    // 站位
  }

  render() {
    return (
      <div className="container" id="home">
        <header><Header /></header>
        <div className="main">
          <div className="wait-make scroll-wrap">
            <div className="title">等待列表</div>
            <ReactIscroll
              iScroll={iScroll}
              className="iscroll"
              options={this.props.options}
            >
              <div className="scroll-inner-box">
                {
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map(() => <WaitMake />)
                }
              </div>
            </ReactIscroll>
          </div>
          <div className="making scroll-wrap">
            <div className="title">制作中</div>
            <ReactIscroll
              iScroll={iScroll}
              className="iscroll"
              options={this.props.options}
            >
              <div className="scroll-inner-box">
                {
                  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].map(() => <Making />)
                }
              </div>
            </ReactIscroll>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

Home.defaultProps = {
  options: {
    mouseWheel: true,
    scrollX: true,
  },
}
Home.propTypes = {
  options: PropTypes.object.isRequired
}

export default connect(state => ({
  a: state,
}))(Home)
