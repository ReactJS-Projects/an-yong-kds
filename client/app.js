import React from 'react'
import { render } from 'react-dom'
import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'
import { AppContainer } from 'react-hot-loader' // eslint-disable-line
import Component from './routers'
import store, { history } from './redux/stores'
import './style.less'

render(
  <AppContainer>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Component />
      </ConnectedRouter>
    </Provider>
  </AppContainer>,
  document.querySelector('#root'),
)

if (module.hot) {
  module.hot.accept()
}

